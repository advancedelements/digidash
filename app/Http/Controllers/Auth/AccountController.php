<?php

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    public function logout()
    {
        Auth::guard()->logout();
        return redirect('/');
    }

    public function check()
    {
        $loggedin = array(
            'user' => Auth::guard()->check()?"admin":"guest"
        );

        return response()->json($loggedin);
    }
}