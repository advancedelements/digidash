<?php

namespace App\IotServer;

use Askedio\LaravelRatchet\RatchetWsServer;
use Illuminate\Support\Facades\Auth;
use Ratchet\ConnectionInterface;

class IotServer extends RatchetWsServer
{
    private $guests;
    private $admins;
    public function onEntry($entry)
    {
        $this->sendAll($entry[1]);
    }

    public function getPayload($defaultMsg = "ack")
    {
        return json_encode(array( 'command' => $defaultMsg));
    }

    public function __construct($console)
    {
        parent::__construct($console);
        $this->guests = new \SplObjectStorage();
        $this->admins = new \SplObjectStorage();
    }

    public function onMessage(ConnectionInterface $conn, $input)
    {
        parent::onMessage($conn, $input);
        //$this->sendAll(sprintf('Message from %d: %s', $conn->resourceId, $input));
        $payload = json_decode($input);
        var_dump($payload);
        if(isset($payload->user)){
            if ($payload->user == 'admin') {
                $this->admins->attach($conn);
                $conn->send(count($this->guests));
            }

            if ($payload->user == 'guest') {
                $this->guests->attach($conn);
            }
        }

        if(isset($payload->command) && $payload->command == "get-report"){
            $this->cmdGuestInfo($payload->data);
        }

        if(isset($payload->command) && $payload->command == "send-report"){
            $this->sendAllAdmins($this->cmdSendReport($payload->data));
        }

        if(isset($payload->command) && $payload->command == "rebuild"){
            $this->sendAllAdmins($this->cmdRebuild());
        }
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $this->conn = $conn;

        $this->attach();
        $conn->send($conn->resourceId);
    }

    private function attach()
    {
        $this->clients->attach($this->conn);
        $this->console->info(sprintf('Connected: %d', $this->conn->resourceId));

        $this->connections = count($this->clients);
        $this->console->info(sprintf('%d %s', $this->connections, str_plural('connection', $this->connections)));

        return $this;
    }

    public function onClose(ConnectionInterface $conn)
    {
        $this->clients->detach($conn);
        $this->admins->detach($conn);
        $this->guests->detach($conn);
        $this->sendAllAdmins($this->cmdRebuild());
    }

    public function sendAllAdmins($message)
    {
        foreach ($this->admins as $admin) {
            $admin->send($message);
        }
    }

    public function cmdRebuild()
    {
        $data = array();
        $data['command'] = "rebuild";
        $data['clients'] = array();
        foreach ($this->guests as $guest) {
            $data['clients'][] = $guest->resourceId;
        }
        return json_encode($data);
    }

    public function cmdGuestInfo($resourceId)
    {
        $payload = array();
        $payload['command'] = "get-report";

        foreach ($this->guests as $guest) {
            if ($guest->resourceId == $resourceId) {
                $guest->send(json_encode($payload));
            }
        }
    }

    public function cmdSendReport($msg="")
    {
        $payload = array();
        $payload['command'] = "send-report";
        $payload['data'] = $msg;

        return json_encode($payload);
    }
}
