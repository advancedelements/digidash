export default class AccountManager {
    init() {

        return new Promise(function(resolve, reject) {
            $.ajax({
                url: '/check-logged-in',
                contentType: 'application/json; charset=utf-8'
            }).done(resolve).fail(reject);
        });
    }
}