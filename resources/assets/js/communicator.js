import AccountManager from './accountManager';


class Communicator {
    constructor(user){
        this.user = user;
    }
    connect() {
        return new Promise(function(resolve, reject){
            let conn = new WebSocket("ws://"+ window.location.hostname+":8080");
            conn.onopen = function(e) {
                console.log("Connection established!");
                console.log(e);
                resolve(conn);
            };
            conn.onmessage = function(e) {
                var response = JSON.parse(e.data);
                switch (response.command) {
                    case "get-report" :
                        conn.send(JSON.stringify({
                            command: 'send-report',
                            data : navigator.userAgent,
                        }));
                        break;
                    case "rebuild":
                        var html = '<ul class="list-group list-group-flush">';
                        $.each(response.clients ,function (key, value){
                            html += "<li class=\"list-group-item\"> Client #" + value +
                                "<span class=\"badge badge-success float-right\"> Online </span> " +
                                "<a data-client-id=\""+value+"\" style=\"cursor: pointer;\" class=\"badge badge-outline-info float-right\"> GetInfo </a>"+
                                "</li>";
                        });
                        html+= "</ul>";
                        $('#client-container').html(html);
                        $('body').on('click','#client-container li',function(){
                            var clientId = $(this).find('a').data('client-id');
                            conn.send(JSON.stringify({
                                command: 'get-report',
                                data: clientId
                            }));
                        });
                        break;
                    case 'send-report':
                        $('#userAgentModal .modal-body').html(response.data);
                        $('#userAgentModal').modal('show')
                        break;
                }
            };
            conn.onerror = function (err) {
                reject(err);
            };
        });
    }
}

let accountMgr = new AccountManager();
accountMgr.init().then(function(data) {
        var commMgr = new Communicator(data.user).connect().then(function(server){
            server.send(JSON.stringify({
                user: data.user,
                command:  "rebuild"
            }));
        });
});
