@extends('layouts.app')

@section('content')
<div class="container dashboard">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Admin Dashboard
                </div>
                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif

                    <div id="client-container">
                        Loading Clients ...
                        <!--<ul class="list-group list-group-flush">
                            <li class="list-group-item"> Client #431

                                <span class="badge badge-success float-right"> Online </span>
                                <span data-client-id="" class="badge badge-outline-info float-right"> GetInfo </span>
                            </li>
                        </ul> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="userAgentModal" tabindex="-1" role="dialog" aria-labelledby="userAgentModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="userAgentModalLabel">Client User Agent is </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
