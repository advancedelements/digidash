<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/logout', 'Auth\AccountController@logout')->name('logout');
Route::get('/check-logged-in', 'Auth\AccountController@check')->name('check-logged-in');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
